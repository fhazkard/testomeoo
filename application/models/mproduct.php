<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Mproduct extends CI_Model
{
	public function get_alldata()
	{
		return $this->db->get('tbl_products')->result_array();
	}

	public function collect_typeproduct()
	{
		return $this->db->query("SELECT COUNT(*) AS qty, type FROM tbl_products GROUP BY type")->result_array();
	}
}
