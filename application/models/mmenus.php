<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Mmenus extends CI_Model
{
	public function get_alldata()
	{
		return $this->db->get('tbl_menus')->result_array();
	}

	public function verify_menu($uniqname)
	{
		$aray = array('nama_idmenu' => $uniqname);
		$this->db->select('menu_id,active');
		$this->db->where($aray);
		return $this->db->get('tbl_menus')->row();
	}

	public function update_data($id, $nama_idmenu, $active)
	{
		$result = 'Success';
		$menus['data'] =  $this->check_existdata($nama_idmenu);
		if (count($menus) > 0 && $menus['data'] != null && $menus['data']->active == $active) {
			return 'Duplicate Name';
		}

		$this->db->trans_start();
		$data = array(
			'nama_idmenu' => $nama_idmenu,
			'active' => $active
		);
		$this->db->where('menu_id', $id);
		$this->db->update('tbl_menus', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}

	public function check_existdata($nama_idmenu)
	{
		$aray = array('nama_idmenu' => $nama_idmenu);
		$this->db->select('*');
		$this->db->where($aray);
		return $this->db->get('tbl_menus')->row();
	}

	public function insert_data($nama_idmenu)
	{
		$result = 'Success';
		$menus['data'] =  $this->check_existdata($nama_idmenu);
		if (count($menus) > 0 && $menus['data'] != null) {
			return 'Duplicate Name';
		}

		$this->db->trans_start();
		$data = array(
			'nama_idmenu' => $nama_idmenu,
		);
		$this->db->insert('tbl_menus', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}
}
