<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Mrole extends CI_Model
{
	public function get_alldata()
	{
		return $this->db->get('tbl_roles')->result_array();
	}

	public function get_data($id)
	{
		$this->db->select('role_nama');
		return $this->db->get_where('tbl_roles', array('role_id' => $id))->row();
	}

	public function update_data($id, $role_nama, $active)
	{
		$result = 'Success';
		$this->db->trans_start();
		$data = array(
			'role_nama' => $role_nama,
			'active' => $active
		);
		$this->db->where('role_id', $id);
		$this->db->update('tbl_roles', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}

	public function insert_data($role_nama)
	{
		$result = 'Success';
		$this->db->trans_start();
		$data = array(
			'role_nama' => $role_nama,
		);
		$this->db->insert('tbl_roles', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}
}
