<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Muser extends CI_Model
{
	public function get_alldata()
	{
		return $this->db->get('tbl_users')->result_array();
	}

	public function join_alldata()
	{
		$this->db->select('tbl_users.user_id,tbl_users.user_nama,tbl_users.email,tbl_users.user_role,tbl_users.active,tbl_roles.role_nama');
		$this->db->from('tbl_users');
		$this->db->join('tbl_roles', 'tbl_roles.role_id = tbl_users.user_role');
		return $this->db->get()->result_array();
	}

	public function login_user($usernama, $pass)
	{
		$aray = array('user_nama' => $usernama, 'pass' => $pass);
		$this->db->select('user_id, user_nama, user_role, active');
		$this->db->where($aray);
		return $this->db->get('tbl_users')->row();
	}

	public function change_pass($id, $pass)
	{
		$result = 'Success';
		$this->db->trans_start();
		$data = array(
			'pass' => $pass,
		);
		$this->db->where('user_id', $id);
		$this->db->update('tbl_users', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}

	public function insert_data($user_id, $usernama, $pass, $email, $role_id)
	{
		$result = 'Success';
		$this->db->trans_start();
		$data = array(
			'user_id' => $user_id,
			'user_nama' => $usernama,
			'pass' => $pass,
			'email' => $email,
			'user_role' => $role_id
		);
		$this->db->insert('tbl_users', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}

	public function update_data($user_id, $usernama, $email, $role_id)
	{
		$result = 'Success';
		$this->db->trans_start();
		$data = array(
			'user_nama' => $usernama,
			'email' => $email,
			'user_role' => $role_id,
		);
		$this->db->where('user_id', $user_id);
		$this->db->update('tbl_users', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}

	public function reactive_user($user_id, $active)
	{
		$result = 'Success';
		$this->db->trans_start();
		$data = array(
			'active' => $active,
		);
		$this->db->where('user_id', $user_id);
		$this->db->update('tbl_users', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}

	public function reset_pass($user_id, $pass)
	{
		$result = 'Success';
		$this->db->trans_start();
		$data = array(
			'pass' => $pass,
		);
		$this->db->where('user_id', $user_id);
		$this->db->update('tbl_users', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}
}
