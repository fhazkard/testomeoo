<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Maccess extends CI_Model
{
	public function get_alldata()
	{
		$this->db->select('*, tbl_access.active AS activ');
		$this->db->from('tbl_access');
		$this->db->join('tbl_roles', 'tbl_roles.role_id = tbl_access.role_id');
		$this->db->join('tbl_menus', 'tbl_menus.menu_id = tbl_access.menu_id');
		return $this->db->get()->result_array();
	}

	public function get_selectdata($scale)
	{
		$aray = array('tbl_access.role_id' => $scale);
		$this->db->select('*, tbl_access.active AS activ');
		$this->db->from('tbl_access');
		$this->db->join('tbl_roles', 'tbl_roles.role_id = tbl_access.role_id');
		$this->db->join('tbl_menus', 'tbl_menus.menu_id = tbl_access.menu_id');
		$this->db->where($aray);
		return $this->db->get()->result_array();
	}

	public function get_data($role_id)
	{
		$aray = array('role_id' => $role_id, 'tbl_access.active' => 1, 'tbl_menus.active' => 1);
		$this->db->select('*');
		$this->db->from('tbl_access');
		$this->db->join('tbl_menus', 'tbl_menus.menu_id = tbl_access.menu_id');
		$this->db->where($aray);
		return $this->db->get()->row();
	}

	public function verify_access($role_id, $menu_id)
	{
		$aray = array('role_id' => $role_id, 'menu_id' => $menu_id);
		$this->db->select('*');
		$this->db->where($aray);
		return $this->db->get('tbl_access')->row();
	}

	public function update_data($access_id, $role_id, $menu_id, $active)
	{
		$result = 'Success';
		$access['data'] =  $this->verify_access($role_id, $menu_id);
		if (count($access) > 0 && $access['data'] != null && $access['data']->active == $active) {
			return 'Duplicate Name';
		}

		$this->db->trans_start();
		$data = array(
			'menu_id' => $menu_id,
			'active' => $active
		);
		$this->db->where('access_id', $access_id);
		$this->db->update('tbl_access', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return 'System Not Respond';
		} else {
			return $result;
		}
	}

	public function insert_data($role_id, $listmenu)
	{
		$result = "Success";
		foreach ($listmenu as $menu_id) {	
			$access['data'] =  $this->verify_access($role_id, $menu_id);
			if ($access['data'] == null) {
				$this->db->trans_start();
				$data = array(
					'role_id' => $role_id,
					'menu_id' => $menu_id,
				);
				$this->db->insert('tbl_access', $data);
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE) {
					return 'System Not Respond';
				}
			}
		}
		return $result;
	}
}
