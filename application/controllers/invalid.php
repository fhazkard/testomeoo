<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Invalid extends CI_Controller
{
	public function index()
	{
		$this->load->view('layouts/v_invalid');
	}
}
