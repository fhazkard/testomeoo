<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Product extends CI_Controller
{
	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SESSION['login'] === 1) {
			$uniqname = "product";
			$role_id = null;
			if (isset($_SESSION['user_id'])) {
				$role_id = $_SESSION['role_id'];
			} else {
				redirect('timeout');
			}

			$this->load->library('sysapp');
			$result = $this->sysapp->verify($role_id, $uniqname);

			if ($result == 'Ok') {
				$this->load->view('layouts/header');
				$this->load->view('v_product');
			} else {
				redirect('invalid');
			}
		} else {
			redirect('timeout');
		}
	}

	public function load_product()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$respon['data'] = $this->mproduct->get_alldata();
		if ($respon['data'] != null) {
			$respon['note'] = "Success";
			echo json_encode($respon);
		} else {
			$respon['note'] = "Data Not Found";
			echo json_encode($respon);
		}
	}

	public function top_typeproduct()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$respon['data'] = $this->mproduct->collect_typeproduct();
		if ($respon['data'] != null) {
			$respon['note'] = "Success";
			echo json_encode($respon);
		} else {
			$respon['note'] = "Data Not Found";
			echo json_encode($respon);
		}
	}
}
