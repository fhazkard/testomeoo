<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Dashboard extends CI_Controller
{
	public function home()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SESSION['login'] === 1) {
			$uniqname = "dashboard";
			$role_id = null;
			if (isset($_SESSION['user_id'])) {
				$role_id = $_SESSION['role_id'];
			} else {
				redirect('timeout');
			}

			$this->load->library('sysapp');
			$result = $this->sysapp->verify($role_id, $uniqname);

			if ($result == 'Ok') {
				$this->load->view('layouts/header');
				$this->load->view('v_dashboard');
			} else {
				redirect('invalid');
			}
		} else {
			redirect('timeout');
		}
	}

	public function logout()
	{
		$this->load->library('sysapp');
		$this->sysapp->off();
		$this->load->view('layouts/v_logout');
	}

	public function changepass()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$user_id = $this->input->post('user_id');
		$password = $this->input->post('password');

		$pass = md5($password);
		$result = $this->muser->change_pass($user_id, $pass);

		if ($result == "Success") {
			$respon['note'] = $result;
			echo json_encode($respon);
		} else {
			$respon['note'] = "System Not Respond";
			echo json_encode($respon);
		}
	}
}
