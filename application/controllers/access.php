<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Access extends CI_Controller
{
	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SESSION['login'] === 1) {
			$uniqname = "access";
			$role_id = null;
			if (isset($_SESSION['user_id'])) {
				$role_id = $_SESSION['role_id'];
			} else {
				redirect('timeout');
			}

			$this->load->library('sysapp');
			$result = $this->sysapp->verify($role_id, $uniqname);

			if ($result == 'Ok') {
				$this->load->view('layouts/header');
				$this->load->view('v_accesspanel');
			} else {
				redirect('invalid');
			}
		} else {
			redirect('timeout');
		}
	}

	public function load_data()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();
		
		$respon['data'] = null;
		$scale = $this->input->post('scale');
		if($scale == 0){
			$respon['data'] = $this->maccess->get_alldata();
		}else{
			$respon['data'] = $this->maccess->get_selectdata($scale);
		}
	
		if ($respon['data'] != null) {
			$respon['note'] = 'Success';
			echo json_encode($respon);
		} else {
			$respon['note'] = 'Data Not Found';
			echo json_encode($respon);
		}
	}

	public function updatedata()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$access_id = $this->input->post('access_id');
		$role_id = $this->input->post('role_id');
		$menu_id = $this->input->post('menu_id');
		$active = $this->input->post('active');
		$result = $this->maccess->update_data($access_id, $role_id, $menu_id, $active);

		$respon['note'] = $result;
		echo json_encode($respon);
	}

	public function registerdata()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$role_id = $this->input->post('role_id');
		$listmenu = $this->input->post('listmenu');
		$listmenu = json_decode(stripslashes($listmenu));
		$result = $this->maccess->insert_data($role_id,$listmenu);

		$respon['note'] = $result;
		echo json_encode($respon);
	}
}
