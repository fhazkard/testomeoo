<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Menus extends CI_Controller
{
	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SESSION['login'] === 1) {
			$uniqname = "menus";
			$role_id = null;
			if (isset($_SESSION['user_id'])) {
				$role_id = $_SESSION['role_id'];
			} else {
				redirect('timeout');
			}

			$this->load->library('sysapp');
			$result = $this->sysapp->verify($role_id, $uniqname);

			if ($result == 'Ok') {
				$this->load->view('layouts/header');
				$this->load->view('v_menupanel');
			} else {
				redirect('invalid');
			}
		} else {
			redirect('timeout');
		}
	}

	public function load_data()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$respon['data'] = $this->mmenus->get_alldata();
		if ($respon['data'] != null) {
			$respon['note'] = 'Success';
			echo json_encode($respon);
		} else {
			$respon['note'] = 'Data Not Found';
			echo json_encode($respon);
		}
	}

	public function updatedata()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$menu_id = $this->input->post('menu_id');
		$nama_idmenu = $this->input->post('nama_idmenu');
		$active = $this->input->post('active');
		$result = $this->mmenus->update_data($menu_id, $nama_idmenu, $active);

		$respon['note'] = $result;
		echo json_encode($respon);
	}

	public function registerdata()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$nama_idmenu = $this->input->post('nama_idmenu');
		$result = $this->mmenus->insert_data($nama_idmenu);

		$respon['note'] = $result;
		echo json_encode($respon);
	}
}
