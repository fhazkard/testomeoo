<?php
defined('BASEPATH') || exit('No direct script access allowed');
/* use phpmailer\phpmailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use phpmailer\phpmailer\SMTP;

require APPPATH.'third_party/phpmailer/exception.php';
require APPPATH.'third_party/phpmailer/phpmailer.php';
require APPPATH.'third_party/phpmailer/smtp.php'; */
class Users extends CI_Controller
{
	function  __construct(){
        parent::__construct();
	}
	
	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SESSION['login'] === 1) {
			$uniqname = "users";
			$role_id = null;
			if (isset($_SESSION['user_id'])) {
				$role_id = $_SESSION['role_id'];
			} else {
				redirect('timeout');
			}

			$this->load->library('sysapp');
			$result = $this->sysapp->verify($role_id, $uniqname);

			if ($result == 'Ok') {
				$this->load->view('layouts/header');
				$this->load->view('v_loginpanel');
			} else {
				redirect('invalid');
			}
		} else {
			redirect('timeout');
		}
	}

	public function load_data()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$respon['data'] = $this->muser->join_alldata();
		if ($respon['data'] != null) {
			$respon['note'] = 'Success';
			echo json_encode($respon);
		} else {
			$respon['note'] = 'Data Not Found';
			echo json_encode($respon);
		}
	}

	public function registerdata()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$user_id = $this->sysapp->make_uniqID();
		$usernama = $this->input->post('usernama');
		$pass = md5('123456');
		$email = $this->input->post('email');
		$role_id = $this->input->post('role_id');
		$result = $this->muser->insert_data($user_id, $usernama, $pass, $email, $role_id);

		$respon['note'] = $result;
		echo json_encode($respon);
	}

	public function new_register()
	{
		$this->load->library('sysapp');
		$user_id = $this->sysapp->make_uniqID();
		$usernama = $this->input->post('usernama');
		$pass = $this->input->post('pass');
		$pass = md5($pass);
		$email = $this->input->post('email');
		$role_id = 3;
		$result = $this->muser->insert_data($user_id, $usernama, $pass, $email, $role_id);

		if ($result == "Success") {

			try {
				$tgl = date("Y-m-d H:i:s");
				$this->load->library('phpemailer');
				$mail = $this->phpemailer->load();

				// SMTP configuration
				$mail->isSMTP();
				$mail->Host     = 'smtp.gmail.com'; //sesuaikan sesuai nama domain hosting/server yang digunakan
				$mail->SMTPAuth = true;
				$mail->Username = 'cikondangtv@gmail.com'; // user email
				$mail->Password = 'test123omeoo'; // password email
				$mail->SMTPSecure = 'ssl';
				$mail->Port     = 465;

				$mail->setFrom('test@omeoo.com', 'Tester'); // user email
				//$mail->addReplyTo('xxx@hostdomain.com', ''); //user email

				// Add a recipient
				$mail->addAddress('technical@omeoo.com'); //email tujuan pengiriman email

				// Email subject
				$mail->Subject = 'Test Email Register'; //subject email

				// Set email format to HTML
				$mail->isHTML(true);

				// Email body content
				$mailContent = nl2br("Information Register"."\r\n".
				$usernama . ' - ' . $email . ' telah melakukan regitrasi pada ' . $tgl);
				$mail->Body = $mailContent;
				$mail->send();
				$respon['mail'] = "Send";
			} catch (Exception $e) {
				$respon['mail'] = $mail->ErrorInfo;
			}
		}
		$respon['note'] = $result;
		echo json_encode($respon);
	}

	public function updatedata()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$user_id = $this->input->post('user_id');
		$usernama = $this->input->post('usernama');
		$email = $this->input->post('email');
		$role_id = $this->input->post('role_id');
		$result = $this->muser->update_data($user_id, $usernama, $email, $role_id);

		$respon['note'] = $result;
		echo json_encode($respon);
	}

	public function reactive()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$user_id = $this->input->post('user_id');
		$active = $this->input->post('active');
		$result = $this->muser->reactive_user($user_id, $active);

		$respon['note'] = $result;
		echo json_encode($respon);
	}

	public function repas()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$pass = md5('123456');
		$user_id = $this->input->post('user_id');
		$result = $this->muser->reset_pass($user_id, $pass);

		$respon['note'] = $result;
		echo json_encode($respon);
	}
}
