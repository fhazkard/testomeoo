<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Timeout extends CI_Controller
{
	public function index()
	{
		$this->load->library('sysapp');
		$this->sysapp->off();
		$this->load->view('layouts/v_timeout');
	}
}
