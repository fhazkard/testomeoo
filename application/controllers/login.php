<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Login extends CI_Controller
{
	public function index()
	{
		$this->load->view('v_login');
	}

	public function akses()
	{
		$usernama = $this->input->post('usernama');
		$pass = $this->input->post('pass');
		$pass = md5($pass);
		$data['user'] = $this->muser->login_user($usernama, $pass);

		if ($data['user'] != null) {
			if ($data['user']->active == 1) {
				$role = $data['user']->user_role;
				$menus['data'] = $this->maccess->get_data($role);

				if ($menus['data'] != null) {
					try {
						$_SESSION['limit'] = time() + 10800;
						$_SESSION['user_id'] = $data['user']->user_id;
						$_SESSION['nama'] = $data['user']->user_nama;

						$_SESSION['role_id'] = $role;
						$data['role'] = $this->mrole->get_data($role);
						$_SESSION['role'] = $data['role']->role_nama;

						$_SESSION['login'] = 1;
						$_SESSION['menus'] = $menus['data']->nama_idmenu;

						$respon['note'] = "Success";
						echo json_encode($respon);
					} catch (\Throwable $e) {
						$respon['note'] = "Error";
						echo json_encode($respon);
					}
				} else {
					$respon['note'] = "Account Dont Have Access";
					echo json_encode($respon);
				}
			} else {
				$respon['user'] = null;
				$respon['note'] = "Account Inactive";
				echo json_encode($respon);
			}
		} else {
			$respon['note'] = "Account Not Found!";
			echo json_encode($respon);
		}
	}
}
