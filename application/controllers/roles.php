<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Roles extends CI_Controller
{
	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SESSION['login'] === 1) {
			$uniqname = "roles";
			$role_id = null;
			if (isset($_SESSION['user_id'])) {
				$role_id = $_SESSION['role_id'];
			} else {
				redirect('timeout');
			}

			$this->load->library('sysapp');
			$result = $this->sysapp->verify($role_id, $uniqname);

			if ($result == 'Ok') {
				$this->load->view('layouts/header');
				$this->load->view('v_rolepanel');
			} else {
				redirect('invalid');
			}
		} else {
			redirect('timeout');
		}
	}

	public function load_data()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$respon['data'] = $this->mrole->get_alldata();
		if ($respon['data'] != null) {
			$respon['note'] = 'Success';
			echo json_encode($respon);
		} else {
			$respon['note'] = 'Data Not Found';
			echo json_encode($respon);
		}
	}

	public function updatedata()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$role_id = $this->input->post('role_id');
		$role_nama = $this->input->post('role_nama');
		$active = $this->input->post('active');
		$result = $this->mrole->update_data($role_id, $role_nama, $active);

		$respon['note'] = $result;
		echo json_encode($respon);
	}

	public function registerdata()
	{
		$this->load->library('sysapp');
		$this->sysapp->recheck();

		$role_nama = $this->input->post('role_nama');
		$result = $this->mrole->insert_data($role_nama);

		$respon['note'] = $result;
		echo json_encode($respon);
	}
}
