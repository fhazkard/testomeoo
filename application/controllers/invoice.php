<?php
defined('BASEPATH') || exit('No direct script access allowed');
class Invoice extends CI_Controller
{
	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SESSION['login'] === 1) {
			$uniqname = "invoice";
			$role_id = null;
			if (isset($_SESSION['user_id'])) {
				$role_id = $_SESSION['role_id'];
			} else {
				redirect('timeout');
			}

			$this->load->library('sysapp');
			$result = $this->sysapp->verify($role_id, $uniqname);

			if ($result == 'Ok') {
				$this->load->view('layouts/header');
				$this->load->view('v_invoice');
			} else {
				redirect('invalid');
			}
		} else {
			redirect('timeout');
		}
	}
}
