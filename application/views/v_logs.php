        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Data Logs</h2>
                            <input id="nama-dokumen" type="hidden" value="Data Logs">
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table aria-describedby="List Log Action" id="tabel_logs" class="table table-striped table-bordered table-hover dataTables-dashboard">
                                    <thead>
                                        <tr>
                                            <th scope="col">Log ID</th>
                                            <th scope="col">Log Description</th>
                                            <th scope="col">Action Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
        </div><!-- right col -->

        </div><!-- main container -->
        </div><!-- body container -->

        <script src="<?php echo base_url() ?>resource/js/allscript.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>resource/js/view/logs.js" type="text/javascript"></script>
        </body>

        </html>