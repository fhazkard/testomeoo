        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Data Order (Dummy IU Only)</h2>
                            <input id="nama-dokumen" type="hidden" value="Data Order">
                            <div class="clearfix"></div>
                        </div>
                        <!-- <div class="x_content">
                            <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target=".tambah" data-backdrop="static" data-keyboard="false"><em class="fa fa-plus-square"></em> Add Menu</a>
                            <div class="table-responsive">
                                <table aria-describedby="List Menu" id="tabel_menu_panel" class="table table-striped table-bordered table-hover dataTables-dashboard">
                                    <thead>
                                        <tr>
                                            <th scope="col">Menu ID</th>
                                            <th scope="col">Menu Name</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <hr />

            <div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                            <h4 class="modal-title" id="">Add Menu</h4>
                        </div>
                        <div class="modal-body">
                            <form id="form_tambah_menu" class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Menu Name:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <input type="text" id="menu_nama_add" class="form-control" placeholder="Input Menu Name (Unique)" required autofocus maxlength="51" minlength="4" onblur="this.value=removeSpaces(this.value);">
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
                            <button type="submit" class="btn btn-success">Add Menu</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>


            <!-- Modal untuk Edit -->
            <div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                            <h4 class="modal-title" id="">Edit Detail Menu</h4>
                        </div>
                        <div class="modal-body">
                            <form id="form_edit_menu" class="form-horizontal form-label-left">
                                <input id="menu_id" type="hidden">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Menu Name:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <input type="text" id="menu_nama_edit" class="form-control" placeholder="Input Menu Name" required autofocus maxlength="51" minlength="4" onblur="this.value=removeSpaces(this.value);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Status:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <select id="active" class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- right col -->

        </div><!-- main container -->
        </div><!-- body container -->

        <script src="<?php echo base_url() ?>resource/js/allscript.js" type="text/javascript"></script>
        <!-- <script src="<?php echo base_url() ?>resource/js/view/menupanel.js" type="text/javascript"></script> -->
        </body>

        </html>