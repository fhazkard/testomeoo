        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Data Access</h2>
                            <input id="nama-dokumen" type="hidden" value="Data Access">
                        </div>
                        <div class="x_content">
                            <row class="col-md-12" style="padding-left:0px;">
                                <div class="col-md-8" style="padding-left:0px;">
                                    <div class="col-md-12" style="padding-left:0px;padding-bottom:4px;">
                                        <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12" style="padding-left:0px;">Select Role:</label>
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                            <select id="select_role" class="form-control">
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding-left:0px;padding-top:4px;">
                                        <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12" style="padding-left:0px;">Select Menu:</label>
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                            <select id="select_menu" name="menu[]" multiple="multiple" class="form-control"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <a id="add_access" class="btn btn-success btn-sm"><em class="fa fa-plus-square"></em> Add Access</a>
                                </div>
                            </row>
                            <div class="clearfix"></div>
                            <br>
                            <div class="table-responsive">
                                <table aria-describedby="List Access" id="tabel_access_panel" class="table table-striped table-bordered table-hover dataTables-dashboard">
                                    <thead>
                                        <tr>
                                            <th scope="col">Access ID</th>
                                            <th scope="col">Role Name</th>
                                            <th scope="col">Menu</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
             <!-- Modal untuk Edit -->
             <div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                            <h4 class="modal-title" id="">Edit Detail Access</h4>
                        </div>
                        <div class="modal-body">
                            <form id="form_edit_access" class="form-horizontal form-label-left">
                                <input id="access_id" type="hidden">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Select Menu:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                    <select id="select_menu_edit" class="form-control"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Status:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <select id="active" class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- right col -->

        </div><!-- main container -->
        </div><!-- body container -->

        <script src="<?php echo base_url() ?>resource/js/allscript.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>resource/js/view/accesspanel.js" type="text/javascript"></script>
        </body>

        </html>