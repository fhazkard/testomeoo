<?php
defined('BASEPATH') || exit('No direct script access allowed');
if (isset($_SESSION["limit"]) && isset($_SESSION["user_id"])) {
    $limit = $_SESSION["limit"];
    if (time() < $limit) {
        redirect('dashboard');
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta, title, favicons -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test Omeoo - Login Page</title>
    <link rel="icon" href="<?php echo base_url() ?>resource/img/favicon.ico">
    <link href="<?php echo base_url() ?>resource/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>resource/css/all.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>resource/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>resource/css/custom/style-login.css" rel="stylesheet">
    <script src="<?php echo base_url() ?>resource/js/custom/path.js" type="text/javascript"></script>
    <link href="<?php echo base_url() ?>resource/css/sweetalert.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Modak|Montserrat+Alternates:700" rel="stylesheet">
</head>

<body class="gray-bg">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>LOGIN</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url(); ?>">Home</a>
                </li>
                <li class="active">
                    <strong>Login Page</strong>
                </li>
            </ol>
        </div>
        <div class="col-sm-8">
            <div class="title-action">
                <a href="#" data-toggle="modal" data-target=".tambah" data-backdrop="static" data-keyboard="false" class="btn btn-primary btn-sm"><em class="fa fa-plus"></em> Register Account</a>
            </div>
        </div>
    </div>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div class="login">
            <div>
                <h1 class="logo-name" style="font-family: 'Montserrat Alternates', sans-serif;font-size:40px;letter-spacing: 0px; color:#1ab394;">DASHBOARD</h1>
            </div>
            <p>Login Your Account!</p>
            <form id="form_login" class="m-t" role="form" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control" id="userlogin" placeholder="Username/Email" autofocus required maxlength="16" minlength="4" onblur="this.value=removeSpaces(this.value);">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="passlogin" placeholder="Password" required maxlength="16" minlength="4">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b"><em class="fas fa-sign-in-alt"></em> Login</button>
            </form>
            <p class="m-t"> <small>&copy; 2020 - Build By Fhazkard.</small> </p>
        </div>
    </div>

    <div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="display:grid;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Register New Account</h4>
                </div>
                <div class="modal-body">
                    <form id="form_register_account" class="form-horizontal form-label-left">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                    <input type="text" id="user_nama" class="form-control" placeholder="Input Username Login" required autofocus onblur="this.value=removeSpaces(this.value);" title="Input Username No Space">
                                    <small style="color:red;">*Required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Email:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                    <input type="email" id="email_add" class="form-control" placeholder="Input Valid Email" required autofocus maxlength=50 minlength=8 title="You Must Verification Email!">
                                    <small style="color:red;">*Required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Password:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                    <input type="password" id="user_pass" class="form-control" placeholder="Input Strong Password" required autofocus title="Please Use Symbol Character">
                                    <small style="color:red;">*Required</small>
                                </div>
                            </div>                
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Confirm Password:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                    <input type="password" id="pass_confirm" class="form-control" placeholder="Input Confirm Password" required autofocus title="Please Use Symbol Character">
                                    <small style="color:red;">*Required</small>
                                </div>
                            </div>                          
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
                    <button type="submit" class="btn btn-primary">Register Account</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url() ?>resource/js/main/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>resource/js/main/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>resource/js/main/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>resource/js/function/make_token.js"></script>
    <script src="<?php echo base_url() ?>resource/js/function/currentdate.js"></script>
    <script src="<?php echo base_url() ?>resource/js/function/remove_space.js"></script>
    <script src="<?php echo base_url() ?>resource/js/view/logins.js" type="text/javascript"></script>
</body>

</html>