<?php
defined('BASEPATH') || exit('No direct script access allowed');
if (isset($_SESSION['limit']) && isset($_SESSION['user_id'])) {
	$limit = $_SESSION['limit'];
	$user_id = $_SESSION['user_id'];
	$nama = $_SESSION['nama'];
	$role_id = $_SESSION['role_id'];
	$role = $_SESSION['role'];

	if (time() > $limit) {
		unset($_SESSION['limit']);
		unset($_SESSION['user_id']);
		unset($_SESSION['nama']);
		unset($_SESSION['role_id']);
		unset($_SESSION['role']);
		session_destroy();
		redirect('timeout');
	}
} else {
	redirect('timeout');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Meta, title, favicons -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Test Omeoo - Dashboard</title>
	<link rel="icon" href="<?php echo base_url() ?>resource/img/favicon.ico">
	<script src="<?php echo base_url() ?>resource/js/custom/path.js" type="text/javascript"></script>
	<script>
		var path = getpath();
	</script>
	<script src="<?php echo base_url() ?>resource/js/headscripts.js" type="text/javascript"></script>
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view" tabindex="0" style="overflow: hidden; outline: none; cursor: grab;">
					<div class="navbar nav_title" style="border: 0;">
						<a href="<?php echo base_url() ?>dashboard" class="site_title"><em class="fab fa-slack"></em> <span>Omeoo</span></a>
					</div>
					<div class="clearfix"></div>
					<!-- menu prile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="<?php echo base_url() ?>resource/img/user.jpg" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<h3 style="margin-bottom:5px;">Welcome,</h3>
							<h3 id="nama_user_login"><?php echo $nama ?></h3>
							<h3 style="margin-top:5px;"><?= date('d - M - Y') ?></h3>
							<input type="hidden" id="user_id" value="<?php echo	$user_id; ?>">
							<input type="hidden" id="role_nama" value="<?php echo	$role; ?>">
						</div>
					</div>
					<!-- /menu prile quick info -->
					<br>

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section active">
							<br>
							<div class="clearfix"></div>
							<ul class="nav side-menu">
								<li><a href="dashboard"><em class='fa fa-home'></em> Home </a></li>
								<li><a href="product"><em class='fas fa-gifts'></em> Data Product </a></li>
								<li><a><em class="fas fa-archive"></em> Sales <span class="chevron"><em class="fas fa-chevron-circle-down"></em></span></a>
									<ul class="nav child_menu" style="display: none;">
										<li><a href="invoice"><em class='fas fa-file-invoice'></em> Invoice </a></li>
									</ul>
								</li>
								<li><a><em class="fas fa-cash-register"></em> Purchase <span class="chevron"><em class="fas fa-chevron-circle-down"></em></span></a>
									<ul class="nav child_menu" style="display: none;">
										<li><a href="orders"><em class='fas fa-shopping-cart'></em> Orders </a></li>
									</ul>
								</li>
								<li><a><em class="far fa-address-book"></em> Contact <span class="chevron"><em class="fas fa-chevron-circle-down"></em></span></a>
									<ul class="nav child_menu" style="display: none;">
										<li><a href="congrup"><em class='fas fa-id-card'></em> Contacts & Groups </a></li>
									</ul>
								</li>
								<li><a><em class="fab fa-windows"></em> System Panel <span class="chevron"><em class="fas fa-chevron-circle-down"></em></span></a>
									<ul class="nav child_menu" style="display: none;">
										<li><a href="menus"><em class='fab fa-elementor'></em> Menu Panel </a></li>
										<li><a href="access"><em class='fas fa-vote-yea'></em> Access Panel </a></li>
										<li><a href="roles"><em class='fa fa-user'></em> Role Panel </a></li>
										<li><a href="users"><em class='fa fa-users'></em> Login Panel </a></li>
									</ul>
								</li>
							</ul><!-- side-menu -->
						</div><!-- menu_section -->
					</div><!-- sidebar menu -->
				</div>
				<!--scroll-view -->
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><em class="animated bounceIn fa fa-bars"></em></a>
						</div>
						<ul class="nav navbar-nav navbar-right">
							<li>
								<!-- li -->
								<a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="margin-bottom:0px;">
									Setting <span class="fa fa-cog fa-lg" style="display:inline-block !important; margin-bottom:0px;"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
									<li>
										<a data-toggle="modal" data-target=".ganti_pass" data-backdrop="static" data-keyboard="false" style='padding-left:15px;'> Change Password</a>
									</li>
									<li>
										<a href='<?php echo base_url() ?>dashboard/logout/' style='padding-left:15px;'> Logout</a>
									</li>
								</ul>
							</li><!-- li -->
						</ul>
					</nav>
				</div><!-- nav menu -->
			</div><!-- top navigation -->

			<div class="animated bounceInUp modal fade ganti_pass" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
							</button>
							<h4 class="modal-title">Change Password</h4>
						</div>
						<form id="form_gantipass" method="POST" class="form-horizontal form-label-left">
							<div class="modal-body">
								<div class="form-group">
									<label class="control-label col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">New Password:</label>
									<div class="col-md-8 col-sm-8 col-xs-12" style="padding-left:0px;padding-right:0px;">
										<input type="password" id="password" class="form-control" placeholder="Input New Password" required maxlength="10" minlength="6" autofocus>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Confirmation Password:</label>
									<div class="col-md-8 col-sm-8 col-xs-12" style="padding-left:0px;padding-right:0px;">
										<input type="password" id="konfirm" class="form-control" placeholder="Input Same Password" required maxlength="10" minlength="6">
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
								<button type="submit" class="btn btn-success">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- top navigation -->