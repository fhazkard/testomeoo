        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Data Account Login</h2>
                            <input id="nama-dokumen" type="hidden" value="Data Account Login">
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target=".tambah" data-backdrop="static" data-keyboard="false"><em class="fa fa-plus-square"></em> Add Account</a>
                            <div class="table-responsive">
                                <table aria-describedby="List Account" id="tabel_login_panel" class="table table-striped table-bordered table-hover dataTables-dashboard">
                                    <thead>
                                        <tr>
                                            <th scope="col">User ID</th>
                                            <th scope="col">Username</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Role</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />

            <div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                            <h4 class="modal-title" id="">Add Account Login</h4>
                        </div>
                        <div class="modal-body">
                            <form id="form_tambah_account" class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <input type="text" id="user_nama_add" class="form-control" placeholder="Masukan Username Account" required autofocus maxlength="16" minlength="4" onblur="this.value=removeSpaces(this.value);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Email:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <input type="email" id="email_add" class="form-control" placeholder="Input Valid Email" required autofocus maxlength=50 minlength=8>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Role:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <select id="user_role_add" class="form-control"></select>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
                            <button type="submit" class="btn btn-success">Add Account</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>


            <!-- Modal untuk Edit -->
            <div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                            <h4 class="modal-title" id="">Edit Account Login</h4>
                        </div>
                        <div class="modal-body">
                            <form id="form_edit_account" class="form-horizontal form-label-left">
                                <input id="user_id" type="hidden" name="user_id">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <input type="text" id="user_nama_edit" name="user_nama" class="form-control" placeholder="Masukan Username Account" required autofocus maxlength="30" minlength="4" onblur="this.value=removeSpaces(this.value);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Email:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <input type="email" id="email_edit" class="form-control" placeholder="Input Valid Email" required autofocus maxlength=50 minlength=8>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Role:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-12" style="padding-left:0px;padding-right:0px;">
                                        <select name="user_role" id="user_role_edit" class="form-control"></select>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- right col -->

        </div><!-- main container -->
        </div><!-- body container -->

        <script src="<?php echo base_url() ?>resource/js/allscript.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>resource/js/view/loginpanel.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>resource/js/custom/confirm-suspend.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>resource/js/custom/confirm-repas.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>resource/js/custom/confirm-actived.js" type="text/javascript"></script>
        </body>

        </html>