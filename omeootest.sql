-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2020 at 04:18 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `omeootest`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_access`
--

CREATE TABLE `tbl_access` (
  `access_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_access`
--

INSERT INTO `tbl_access` (`access_id`, `role_id`, `menu_id`, `active`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 7, 1),
(4, 1, 8, 1),
(5, 1, 9, 1),
(6, 1, 10, 1),
(7, 1, 11, 1),
(8, 1, 12, 1),
(9, 1, 13, 1),
(10, 2, 1, 1),
(11, 2, 11, 1),
(12, 2, 12, 1),
(13, 2, 13, 1),
(14, 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menus`
--

CREATE TABLE `tbl_menus` (
  `menu_id` int(11) NOT NULL,
  `nama_idmenu` varchar(51) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menus`
--

INSERT INTO `tbl_menus` (`menu_id`, `nama_idmenu`, `active`) VALUES
(1, 'dashboard', 1),
(2, 'menus', 1),
(7, 'access', 1),
(8, 'roles', 1),
(9, 'users', 1),
(10, 'product', 1),
(11, 'invoice', 1),
(12, 'orders', 1),
(13, 'congrup', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `id` int(11) NOT NULL,
  `organization` int(11) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'product',
  `name` varchar(100) NOT NULL,
  `code` varchar(40) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `cost_price` double DEFAULT NULL,
  `cost_currency` int(11) DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  `sale_currency` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_added` int(10) NOT NULL,
  `user_modified` int(10) DEFAULT NULL,
  `user_deleted` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`id`, `organization`, `type`, `name`, `code`, `description`, `category`, `unit`, `cost_price`, `cost_currency`, `sale_price`, `sale_currency`, `is_deleted`, `date_added`, `date_modified`, `user_added`, `user_modified`, `user_deleted`) VALUES
(1, 12, 'product', 'Product 1', 'PR1-0001', NULL, 0, 0, 50000, 1, 70000, 1, 0, '2019-11-04 07:20:08', '2019-11-04 07:20:08', 1, NULL, NULL),
(2, 12, 'product', 'Product 2', 'PR2-0001', NULL, 0, 0, 5, 2, 7, 2, 0, '2019-11-04 07:21:12', '2019-11-04 07:21:12', 1, NULL, NULL),
(3, 12, 'service', 'Service 1', '', NULL, 0, NULL, NULL, NULL, 10, 2, 0, '2019-11-04 07:24:20', '2019-11-04 07:24:20', 1, NULL, NULL),
(4, 12, 'product', 'Rifky Syaripudin', 'RIS-0001', NULL, NULL, NULL, 2, 2, 4, 2, 0, '2019-11-15 08:11:48', '2019-11-15 08:11:48', 1, NULL, NULL),
(5, 12, 'service', 'Nur Rohman', 'NUR-0001', NULL, NULL, NULL, 0, 2, 10, 2, 0, '2019-11-15 08:12:02', '2019-11-15 08:12:02', 1, NULL, NULL),
(6, 13, 'service', 'Radio Placement', 'RAP-0001', NULL, NULL, NULL, 0, 3, 0, 3, 0, '2019-11-20 17:25:18', '2019-11-20 17:25:18', 2, NULL, NULL),
(7, 13, 'product', 'TV Station Placement', 'TVS-0001', NULL, NULL, NULL, 0, 3, 0, 3, 0, '2019-11-20 18:23:18', '2019-11-20 18:23:18', 2, NULL, NULL),
(8, 12, 'product', 'asdasd', 'ASD-0001', NULL, NULL, NULL, 2, 2, 1, 2, 0, '2019-11-22 09:00:03', '2019-11-22 09:00:03', 1, NULL, NULL),
(9, 13, 'service', 'Pre Production', 'PRP-0001', NULL, 0, 0, 0, 3, 13200000, 3, 0, '2020-01-07 07:26:55', '2020-01-07 07:27:52', 5, 5, NULL),
(10, 13, 'service', 'KV & Brochure', 'KV&-0001', NULL, 0, 1, 0, 3, 0, 3, 0, '2020-01-07 07:39:14', '2020-01-10 10:50:53', 5, 5, NULL),
(11, 13, 'product', 'discount', 'DIS-0001', NULL, NULL, NULL, 0, 3, 0, 3, 0, '2020-02-05 05:11:42', '2020-02-05 05:11:42', 2, NULL, NULL),
(12, 18, 'service', 'service', 'service', NULL, NULL, NULL, 0, 8, 0, 8, 0, '2020-03-05 04:42:59', '2020-03-05 04:42:59', 8, NULL, NULL),
(13, 18, 'service', 'PENGARON', 'PEN-0001', NULL, NULL, NULL, 0, 8, 0, 8, 0, '2020-03-05 04:46:28', '2020-03-05 04:46:28', 8, NULL, NULL),
(14, 18, 'service', 'service 2', 'SE2-0001', NULL, NULL, NULL, 0, 8, 0, 8, 0, '2020-03-05 04:51:15', '2020-03-05 04:51:15', 8, NULL, NULL),
(15, 18, 'service', 'Social Media Maintenance', 'SMM-0001', NULL, NULL, NULL, 0, 8, 0, 8, 0, '2020-03-05 04:55:29', '2020-03-05 04:55:29', 8, NULL, NULL),
(16, 18, 'service', 'Account Management', 'ACM-0001', NULL, NULL, NULL, 0, 8, 0, 8, 0, '2020-03-05 04:55:40', '2020-03-05 04:55:40', 8, NULL, NULL),
(17, 18, 'service', 'Website Maintenance', 'WEM-0001', NULL, NULL, NULL, 0, 8, 0, 8, 0, '2020-03-05 04:55:51', '2020-03-05 04:55:51', 8, NULL, NULL),
(18, 18, 'service', 'Content Creation', 'COC-0001', NULL, NULL, NULL, 0, 8, 0, 8, 0, '2020-03-05 04:56:10', '2020-03-05 04:56:10', 8, NULL, NULL),
(19, 18, 'service', 'Adjusment', 'ADJ-0001', NULL, NULL, NULL, 0, 8, 0, 8, 0, '2020-03-05 04:58:08', '2020-03-05 04:58:08', 8, NULL, NULL),
(20, 17, 'product', 'asdfads', 'ASD-0001', NULL, NULL, NULL, 0, 7, 0, 7, 0, '2020-03-13 11:09:52', '2020-03-13 11:09:52', 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `role_id` int(11) NOT NULL,
  `role_nama` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`role_id`, `role_nama`, `active`) VALUES
(1, 'Admin', 1),
(2, 'Staff', 1),
(3, 'User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` varchar(51) NOT NULL,
  `user_nama` varchar(45) DEFAULT NULL,
  `pass` varchar(55) DEFAULT NULL,
  `email` varchar(51) NOT NULL,
  `user_role` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_nama`, `pass`, `email`, `user_role`, `active`, `timestamp`) VALUES
('TV5y1SxuIAemLk2y8XY44cqnDdN2', 'testome', 'cc03e747a6afbbcbf8be7668acfebee5', 'test@omeoo.com', 1, 1, '2020-05-28 23:15:58'),
('xkuqF056000C7cuoF4eq420tu0IBk0hB222qIJs5D9a48qmD', 'Sanda', 'e10adc3949ba59abbe56e057f20f883e', 'sanda.agungj@gmail.com', 2, 1, '2020-05-28 23:15:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_access`
--
ALTER TABLE `tbl_access`
  ADD PRIMARY KEY (`access_id`);

--
-- Indexes for table `tbl_menus`
--
ALTER TABLE `tbl_menus`
  ADD PRIMARY KEY (`menu_id`),
  ADD UNIQUE KEY `nama_idmenu` (`nama_idmenu`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `role_nama` (`role_nama`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_nama` (`user_nama`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_access`
--
ALTER TABLE `tbl_access`
  MODIFY `access_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_menus`
--
ALTER TABLE `tbl_menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
