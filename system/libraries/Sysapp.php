<?php
defined('BASEPATH') || exit('No direct script access allowed');
class CI_Sysapp
{
    function __construct()
	{
		$this->CI = &get_instance();
	}

	public function recheck()
	{
		if (isset($_SESSION['limit'])) {
			$limit = $_SESSION['limit'];
			if (time() > $limit) {
				unset($_SESSION['limit']);
				unset($_SESSION['user_id']);
				unset($_SESSION['nama']);
				unset($_SESSION['role_id']);
				unset($_SESSION['role']);
				session_destroy();
				redirect("timeout");
			}
		} else {
			redirect("timeout");
		}
	}

	public function off()
	{
		unset($_SESSION['limit']);
		unset($_SESSION['user_id']);
		unset($_SESSION['nama']);
		unset($_SESSION['role_id']);
		unset($_SESSION['role']);
		session_destroy();
	}

	public function make_uniqID()
	{
		$text = "";
		$timestamp = date('YmdHis');
		$possible = "A0B1C2D3E4F5G6H7I8J9" . $timestamp . "acbedfhgjiklponmqutrswvxyz";
		for ($i = 0; $i < 50; $i++) {
			$no =  rand(0, strlen($possible));
			$teks = substr($possible, $no, 1);
			$text = $text . '' . $teks;
		}
		return $text;
	}
    
    public function verify($role_id, $uniqname)
	{
		$menus['data'] = $this->CI->mmenus->verify_menu($uniqname);

		if ($menus['data'] == null) {
			return 'Menu Not Found';
		}
		if ($menus['data']->active == 0) {
			return 'Menu Not Active';
		}

		$menu_id = $menus['data']->menu_id;
		$accces['data'] = $this->CI->maccess->verify_access($role_id,$menu_id);

		if ($accces['data'] == null) {
			return 'Access Invalid';
		}
		if ($accces['data']->active == 0) {
			return 'Access Not Active';
		}
		
		return "Ok";
	}
}

?>