function confirm_repas(id) {
    var url = path + "users/repas";
    $('#form_repas_login_' + id).on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Confirmation Reset Password",
            text: "Are You Sure To Reset Password This Account?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, Sure!',
            cancelButtonText: "No, Cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: { 'id': id },
                        success: function (msg) {
                            var data = JSON.parse(msg);
                            if (data.note == "Success") {
                                swal({ title: "Success", text: "Reset Password Success!", type: "success" },
                                    function () {
                                        window.location = window.location.href;
                                    });
                            } else {
                                swal({ title: "Failed", text: "Reset Password Failed!", type: "error" },
                                    function () {
                                        window.location = window.location.href;
                                    });
                            }
                        }
                    });
                }
                else {
                    swal("Cancelled", "You Cancel Confirmation!", "error");
                }
            });
    });
}
