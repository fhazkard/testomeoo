$(document).ready(function () {
    get_top_typeproduct();
});

function genData(list) {
    var legendData = [];
    var seriesData = [];
    for (var i = 0; i < list.length; i++) {
        legendData.push(list[i]['type']);
        seriesData.push({
            name: list[i]['type'],
            value: list[i]['qty']
        });
    }

    return {
        legendData: legendData,
        seriesData: seriesData,
    };
}

function get_top_typeproduct() {
    var url = path + "product/top_typeproduct";
    $.ajax({
        type: "POST",
        url: url,
        data: { 'web': 'true' },
        success: function (msg) {
            var data = [];
            try {
                data = JSON.parse(msg);
            }
            catch (err) {
                console.log("Data Product Null: ", err.message);
            }
            if (data.note == "Success") {
                $('#graph_top_typeproduct').attr('style', 'width: 100%; height:500px; margin-top:20px;')
                Graph_type(data.data);
            } else {
                console.log("Data Product Empty.: ", data);
            }
        }
    });
}


function Graph_type(list) {
    var data = genData(list);
    var graphpie = echarts.init(document.getElementById('graph_top_typeproduct'), theme);
    var d = new Date();
    d = d.getFullYear();

    graphpie.setOption({
        title: {
            text: 'Most Product Type',
            subtext: 'Graph ' + d
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        toolbox: {
            show: true,
            feature: {
                dataView: { show: true, readOnly: false },
                restore: { show: true },
                saveAsImage: { show: true }
            }
        },
        legend: {
            type: 'scroll',
            orient: 'vertical',
            right: 200,
            top: 20,
            bottom: 0,
            data: data.legendData,
        },
        series: [
            {
                name: 'Type Product',
                type: 'pie',
                radius: '55%',
                center: ['40%', '50%'],
                data: data.seriesData,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    });
}

