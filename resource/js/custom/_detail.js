function formatTableProducts ( data ) {
	// `d` is the original data object for the row
	var table_start ='<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
		'<thead>'+
		'<tr>'+
		'<th>Date Add</th>'+
		'<th>Date Modified</th>'+
		'</tr>'+
		'</thead><tbody>';
	var table_row = '';
	var table_end = '</tbody></table>';
	/* var bl = 'No';
	var vr = "<span style='font-weight: bold;color:red;'>No</span>";
	if(data.blacklist == 1){
		bl = 'Yes';
	}
	if(data.verifikasi == 1){
		vr = "<span style='font-weight: bold;color:green;'>Yes</span>";
	} */
	//for(var x = 0; x < data.qty; x++){
	//	var item = 'item'+x;
		table_row = table_row+
		'<tr class="row_detail">'+
			'<td>'+data.date_added+'</td>'+
			'<td>'+data.date_modified+'</td>'+
        '</tr>';
	//}
    return table_start+table_row+table_end;
}

var formatStandar = {
	exportOptions: {
		format: {
			body: function ( data, column, row, node ) {
				// Strip $ from salary column to make it numeric
				return column > 4 ?
					data.replace( /[$,Rp.]/g, '' ) :
					data;
			},
			footer: function ( data, column, row, node ) {
				// Strip $ from salary column to make it numeric
				if(column === 5){
					return column === 5 ?
					data.replace( /[$,Rp.]/g, '' ) :
					data;
				}else{
					return column < 4 ?
					data.replace(data, '' ) :
					data;
				}
			}
		}
	}
};

var formatPDF = {
	exportOptions: {
		format: {
			footer: function ( data, column, row, node ) {
				// Strip $ from salary column to make it numeric
				return column < 4 ?
				data.replace(data, '' ) :
				data;				
			}
		}
	}
};