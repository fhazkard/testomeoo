function confirm_active(id) {
    var url = path + "users/reactive";
    $('#form_active_login_' + id).on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Confirmation Account Active",
            text: "Are You Sure To Active This Account?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, Sure!',
            cancelButtonText: "No, Cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: { 'user_id': id, 'active': 1 },
                        success: function (msg) {
                            var data = JSON.parse(msg);
                            if (data.note == "Success") {
                                swal({ title: "Success", text: "Actived Account Success!", type: "success" },
                                    function () {
                                        window.location = window.location.href;
                                    });
                            } else {
                                swal({ title: "Failed", text: "Actived Account Failed!", type: "error" },
                                    function () {
                                        window.location = window.location.href;
                                    });
                            }
                        }
                    });
                }
                else {
                    swal("Canceled", "You Cancel Confirmation!", "error");
                }
            });
    });
}


function active_product(id_product, category, active) {
    /*  var user = $('#username_id').text();
     $('#active_product_'+id_product).on('click', function(e) {
     e.preventDefault();
     swal({
         title: "Konfirmasi Change Status Product",
         text: "Apakah anda Yakin Ingin Ubah Status Product Ini?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: '#DD6B55',
         confirmButtonText: 'Ya, Saya Yakin!',
         cancelButtonText: "Tidak, Batalkan!",
         closeOnConfirm: false,
         closeOnCancel: true
     },
     function(isConfirm) {
         if (isConfirm) {
             $.ajax({
                 type: "POST",
                 url: "http://103.246.0.8:9876/api_pos/set_reactive_product.php",
                 data: {'token_key':123, 'id_product':id_product,'category':category,'active':active ,'web':'true','user':user},
                 success: function(msg) {
                     var data = JSON.parse(msg);
                     if(data['note']=="Success"){
                         swal({title: "Success",text: "Status Product Berresult Di Ubah!",type: "success"}, 
                         function() {window.location = "../../dashboard/product_cbp/";
                         });
                     }else{
                         swal({title: "Failed",text: "Status Product Gagal Di Ubah!",type: "error"}, 
                         function() {window.location = "../../dashboard/product_cbp/";
                         });
                     }
                 }
             });
         }
         else {
             swal("Canceled", "Anda Membatalkan Konfirmasi!", "error");
         }
     });
 }); */
}

function inactive_product(id_product, category, active) {
    /* var user = $('#username_id').text();
    $('#inactive_product_'+id_product).on('click', function(e) {
    e.preventDefault();
    swal({
        title: "Konfirmasi Change Status Product",
        text: "Apakah anda Yakin Ingin Ubah Status Product Ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, Saya Yakin!',
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false,
        closeOnCancel: true
    },
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: "http://103.246.0.8:9876/api_pos/set_reactive_product.php",
                data: {'token_key':123, 'id_product':id_product,'category':category,'active':active ,'web':'true','user':user},
                success: function(msg) {
                    var data = JSON.parse(msg);
                    if(data.note=="Success"){
                        swal({title: "Success",text: "Status Product Berresult Di Ubah!",type: "success"}, 
                        function() {window.location = "../../dashboard/product_cbp/";
                        });
                    }else{
                        swal({title: "Failed",text: "Status Product Gagal Di Ubah!",type: "error"}, 
                        function() {window.location = "../../dashboard/product_cbp/";
                        });
                    }
                }
            });
        }
        else {
            swal("Canceled", "Anda Membatalkan Konfirmasi!", "error");
        }
    });
}); */
}

function active_redem(id_redem, active) {
    /* var user = $('#username_id').text();
    $('#active_redem_'+id_redem).on('click', function(e) {
    e.preventDefault();
    swal({
        title: "Konfirmasi Change Status Redeem",
        text: "Apakah anda Yakin Ingin Ubah Status Redeem Ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, Saya Yakin!',
        cancelButtonText: "Tidak, Batalkan!",
        closeOnConfirm: false,
        closeOnCancel: true
    },
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: "http://103.246.0.8:9876/api_pos/set_reactive_redem.php",
                data: {'token_key':123, 'id_redem':id_redem,'active':active ,'web':'true','user':user},
                success: function(msg) {
                    var data = JSON.parse(msg);
                    if(data['note']=="Success"){
                        swal({title: "Success",text: "Status Redeem Berresult Di Ubah!",type: "success"}, 
                        function() {window.location = "../../dashboard/data_redeems/";
                        });
                    }else{
                        swal({title: "Failed",text: "Status Redeem Gagal Di Ubah!",type: "error"}, 
                        function() {window.location = "../../dashboard/data_redeems/";
                        });
                    }
                }
            });
        }
        else {
            swal("Canceled", "Anda Membatalkan Konfirmasi!", "error");
        }
    });
}); */
}

function inactive_redem(id_redem, active) {
    /*  var user = $('#username_id').text();
     $('#inactive_redem_'+id_redem).on('click', function(e) {
     e.preventDefault();
     swal({
         title: "Konfirmasi Change Status Redeem",
         text: "Apakah anda Yakin Ingin Ubah Status Redeem Ini?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: '#DD6B55',
         confirmButtonText: 'Ya, Saya Yakin!',
         cancelButtonText: "Tidak, Batalkan!",
         closeOnConfirm: false,
         closeOnCancel: true
     },
     function(isConfirm) {
         if (isConfirm) {
             $.ajax({
                 type: "POST",
                 url: "http://103.246.0.8:9876/api_pos/set_reactive_redem.php",
                 data: {'token_key':123, 'id_redem':id_redem,'active':active ,'web':'true','user':user},
                 success: function(msg) {
                     var data = JSON.parse(msg);
                     if(data.note=="Success"){
                         swal({title: "Success",text: "Status Redeem Berresult Di Ubah!",type: "success"}, 
                         function() {window.location = "../../dashboard/data_redeems/";
                         });
                     }else{
                         swal({title: "Failed",text: "Status Redeem Gagal Di Ubah!",type: "error"}, 
                         function() {window.location = "../../dashboard/data_redeems/";
                         });
                     }
                 }
             });
         }
         else {
             swal("Canceled", "Anda Membatalkan Konfirmasi!", "error");
         }
     });
 }); */
}

