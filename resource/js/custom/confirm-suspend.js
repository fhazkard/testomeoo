function confirm_suspend(id) {
    var url = path + "users/reactive";
    $('#form_suspend_login_' + id).on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Confirmation Account Suspend",
            text: "Are You Sure To Suspend This Account?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, Sure!',
            cancelButtonText: "No, Cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: { 'user_id': id, 'active': 0 },
                        success: function (msg) {
                            var data = JSON.parse(msg);
                            if (data.note == "Success") {
                                swal({ title: "Success", text: "Suspend Account Success!", type: "success" },
                                    function () {
                                        window.location = window.location.href;
                                    });
                            } else {
                                swal({ title: "Failed", text: "Suspend Account Failed!", type: "error" },
                                    function () {
                                        window.location = window.location.href;
                                    });
                            }
                        }
                    });
                }
                else {
                    swal("Canceled", "You Cancel Confirmation!", "error");
                }
            });
    });
}

