function removeSpaces(string) {
	return string.split(' ').join('');
}

function FindString(string) {
	var tipe = ["jpg","jpeg","png","bmp"];
	var check = -1;
	for(var x=0; x<tipe.length; x++){
		check = string.search(tipe[x]);
		if(check != -1){
			break;
		}
	}
	return check;
}