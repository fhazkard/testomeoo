function currentToday() {
	var today = new Date();
	var dd = addZero(today.getDate());
	var mm = addZero(today.getMonth()+1);
	var yyyy = today.getFullYear();
	return today = yyyy+'-'+mm+'-'+dd;
	//document.getElementById("datefield").setAttribute("max", today);
}

function currentFullToday() {
	var today = new Date();
	var h = addZero(today.getHours());
	var m = addZero(today.getMinutes());
	var d = addZero(today.getDate());
	var mm = today.getMonth();
	var yyyy = today.getFullYear();
	var date = new Date(yyyy, mm, 01);  // 2009-11-10
	var month = date.toLocaleString('default', { month: 'long' });

	return today = d+' '+month+' '+yyyy+' '+h+':'+m;
}

function getDate(sp){
	today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //As January is 0.
	var yyyy = today.getFullYear();
	var time = today.getHours() +today.getMinutes() + today.getSeconds();
	
	if(dd<10) dd='0'+dd;
	if(mm<10) mm='0'+mm;
	return (mm+sp+dd+sp+yyyy+' '+time);
};

function addZero(i) {
	if (i < 10) {
	  i = "0" + i;
	}
	return i;
}

