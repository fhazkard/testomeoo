$(document).ready(function () {
	loadTable();
});

$(document).on("click", "#edit_detail_product", function () {
	var product_id = $(this).data("product_id");
	$(".modal-body #product_id").val(product_id);

	var product_nama = $(this).data("product_nama");
	$(".modal-body #product_nama_edit").val(product_nama);

	var diskon = $(this).data("diskon");
	$(".modal-body #diskon_edit").val(diskon);

	var active = $(this).data("active");
	$(".modal-body #active").val(active);
});

$("#form_tambah_product").submit(function () {
	var nama = $(".modal-body #product_nama_add").val();
	var diskon = $(".modal-body #diskon_add").val();
	var url = path + "product/insert_product";
	$.ajax({
		type: "POST",
		url: url,
		data: { nama: nama, diskon: diskon },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Product Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{
						title: "Success",
						text: "Add New Product Success",
						type: "success",
					},
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Add New Product Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

$("#form_edit_product").submit(function () {
	var id = $(".modal-body #product_id").val();
	var nama = $(".modal-body #product_nama_edit").val();
	var diskon = $(".modal-body #diskon_edit").val();
	var active = $(".modal-body #active").val();
	var url = path + "product/update_type/";
	$.ajax({
		type: "POST",
		url: url,
		data: { product_id: id, nama: nama, diskon: diskon, active: active },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Product Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{
						title: "Success",
						text: "Edit Data Product Success",
						type: "success",
					},
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Edit Data Product Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

function loadTable() {
	swal({
		title: "Collecting Data...",
		text: "Please wait for while...",
		imageUrl: path+"resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});

	var url = path + "product/load_product/";
	$("#tabel_product").DataTable({
		destroy: true,
		paging: true,
		searching: true,
		ordering: true,
		autoWidth: true,
		processing: true,
		//"order":[[0,"desc"]],
		/* columnDefs: [
			{
				targets: 7, 
				className: "text-center",
				width: "5%",
			},
		], */
		//"data" : data,
		ajax: {
			url: url,
			type: "POST",
			data: { web: "true" },
		},
		columns: [
			{
				className: "details-control",
				orderable: false,
				data: null,
				defaultContent: "",
			},
			{
				data: "id",
				visible: false,
			},
			{
				data: "code",
			},
			{
				data: "name",
			},
			{
				data: "cost_price",
				render: function (data, type, row) {
					return "Rp. " + data;
				},
			},
			{
				data: "sale_price",
				render: function (data, type, row) {
					return "Rp. " + data;
				},
			},
			{
				data: "type"
			},
			{
				data: "unit",
			}
		],
	});
	swal.close();
}

$("#tabel_product tbody").on("click", "td.details-control", function () {
	var table = $("#tabel_product").DataTable();
	var tr = $(this).closest("tr");
	var row = table.row(tr);

	if (row.child.isShown()) {
		// This row is already open - close it
		row.child.hide();
		tr.removeClass("shown");
	} else {
		// Open this row
		row.child(formatTableProducts(row.data())).show();
		tr.addClass("shown");
	}
});
