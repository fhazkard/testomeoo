$(document).ready(function () {
	loadTable();
});

$(document).on("click", "#edit_detail_menu", function () {
	var menu_id = $(this).data("menu_id");
	$(".modal-body #menu_id").val(menu_id);

	var nama_idmenu = $(this).data("nama_idmenu");
	$(".modal-body #menu_nama_edit").val(nama_idmenu);

	var active = $(this).data("active");
	$(".modal-body #active").val(active);
});

$("#form_tambah_menu").submit(function () {
	var nama = $(".modal-body #menu_nama_add").val();
	var url = path + "menus/registerdata";
	$.ajax({
		type: "POST",
		url: url,
		data: { nama_idmenu: nama },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Menu Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{ title: "Success", text: "Add New Menu Success", type: "success" },
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Add New Menu Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

$("#form_edit_menu").submit(function () {
	var id = $(".modal-body #menu_id").val();
	var nama = $(".modal-body #menu_nama_edit").val();
	var active = $(".modal-body #active").val();
	var url = path + "menus/updatedata";
	$.ajax({
		type: "POST",
		url: url,
		data: { menu_id: id, nama_idmenu: nama, active: active },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Menu Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{ title: "Success", text: "Edit Data Menu Success", type: "success" },
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Edit Data Menu Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

function loadTable() {
	var url = path + "menus/load_data/";
	$("#tabel_menu_panel").DataTable({
		destroy: true,
		paging: true,
		searching: true,
		ordering: true,
		autoWidth: true,
		processing: true,
		//"order":[[0,"desc"]],
		columnDefs: [
			{
				targets: 3, // your case first column
				className: "text-center",
				width: "5%",
			},
		],
		//"data" : data,
		ajax: {
			url: url,
			type: "POST",
			data: { web: "true" },
		},
		columns: [
			{
				data: "menu_id",
				visible: false,
			},
			{
				data: "nama_idmenu",
			},
			{
				data: "active",
				render: function (data, type, row) {
					if (data == "0") {
						return "<span style='font-weight: bold;color:red;'>Inactive</span>";
					} else {
						return "<span style='font-weight: bold;color:green;'>Active</span>";
					}
				},
			},
			{
				data: null,
				render: function (data, type, row) {
					var list_action =
						"<a id='edit_detail_menu' href='#' class='btn btn-info btn-xs last' " +
						"data-toggle='modal' data-target='.edit' data-backdrop='static' " +
						"data-keyboard='false' style='margin-bottom:0px;float:left;'" +
						"data-menu_id='" +
						data.menu_id +
						"' data-nama_idmenu='" +
						data.nama_idmenu +
						"' data-active='" +
						data.active +
						"'> " +
						"<i class='fas fa-edit'></i> Edit</a> ";
					return list_action;
				},
			},
		],
	});
}
