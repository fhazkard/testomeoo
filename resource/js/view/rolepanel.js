$(document).ready(function () {
	loadTable();
});

$(document).on("click", "#edit_detail_role", function () {
	var role_id = $(this).data("role_id");
	$(".modal-body #role_id").val(role_id);

	var role_nama = $(this).data("role_nama");
	$(".modal-body #role_nama_edit").val(role_nama);

	var active = $(this).data("active");
	$(".modal-body #active").val(active);
});

$("#form_tambah_role").submit(function () {
	var nama = $(".modal-body #role_nama_add").val();
	var url = path + "roles/registerdata";
	$.ajax({
		type: "POST",
		url: url,
		data: { role_nama: nama },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Role Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{ title: "Success", text: "Add New Role Success", type: "success" },
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Add New Role Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

$("#form_edit_role").submit(function () {
	var id = $(".modal-body #role_id").val();
	var nama = $(".modal-body #role_nama_edit").val();
	var active = $(".modal-body #active").val();
	var url = path + "roles/updatedata";
	$.ajax({
		type: "POST",
		url: url,
		data: { role_id: id, role_nama: nama, active: active },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Role Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{ title: "Success", text: "Edit Data Role Success", type: "success" },
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Edit Data Role Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

function loadTable() {
	var url = path + "roles/load_data/";
	$("#tabel_role_panel").DataTable({
		destroy: true,
		paging: true,
		searching: true,
		ordering: true,
		autoWidth: true,
		processing: true,
		//"order":[[0,"desc"]],
		columnDefs: [
			{
				targets: 3, // your case first column
				className: "text-center",
				width: "5%",
			},
		],
		//"data" : data,
		ajax: {
			url: url,
			type: "POST",
			data: { web: "true" },
		},
		columns: [
			{
				data: "role_id",
				visible: false,
			},
			{
				data: "role_nama",
			},
			{
				data: "active",
				render: function (data, type, row) {
					if (data == "0") {
						return "<span style='font-weight: bold;color:red;'>Inactive</span>";
					} else {
						return "<span style='font-weight: bold;color:green;'>Active</span>";
					}
				},
			},
			{
				data: null,
				render: function (data, type, row) {
					var list_action =
						"<a id='edit_detail_role' href='#' class='btn btn-info btn-xs last' " +
						"data-toggle='modal' data-target='.edit' data-backdrop='static' " +
						"data-keyboard='false' style='margin-bottom:0px;float:left;'" +
						"data-role_id='" +
						data.role_id +
						"' data-role_nama='" +
						data.role_nama +
						"' data-active='" +
						data.active +
						"'> " +
						"<i class='fas fa-edit'></i> Edit</a> ";
					return list_action;
				},
			},
		],
	});
}
