$("#form_gantipass").submit(function () {
	var user_id = $("#user_id").val();
	var password = $(".ganti_pass #password").val();
	var konfirm = $(".ganti_pass #konfirm").val();
	var url = path + "dashboard/changepass";

	if (password == konfirm) {
		$.ajax({
			type: "POST",
			url: url,
			data: { user_id: user_id, password: password },
			success: function (msg) {
				var data = [];
				try {
					data = JSON.parse(msg);
				} catch (err) {
					console.log("Data User Null: ", err.message);
				}
				if (data.note == "Success") {
					swal(
						{
							title: "Success",
							text: "Change Password Success.",
							type: "success",
						},
						function () {
							window.location = path + "dashboard/logout";
						}
					);
				} else {
					swal(
						{
							title: "Error",
							text: "Change Password Failed - " + data.note,
							type: "error",
						},
						function () {
							window.location = window.location.href;
						}
					);
					console.log("Error getting documents: ", data);
				}
			},
		});
	} else {
		swal({
			title: "Change Password Failed!",
			text: "Password Not Same",
			type: "warning",
		});
	}
	return false;
});




