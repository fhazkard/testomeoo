var ROLEID = null;

$(document).ready(function () {
	loadRoles();
	loadMenus();
	loadTable(0);
});

$(document).on("click", "#edit_detail_access", function () {
	var access_id = $(this).data("access_id");
	$(".modal-body #access_id").val(access_id);

	ROLEID = $(this).data("role_id");

	var menu_id = $(this).data("menu_id");
	$(".modal-body #select_menu_edit").val(menu_id).trigger('change');

	var active = $(this).data("active");
	$(".modal-body #active").val(active);
});

function loadRoles() {
	var url = path + "roles/load_data/";
	$.ajax({
		type: "POST",
		url: url,
		data: { web: "true" },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Role Null: ", err.message);
			}
			var roles = $.map(data.data, function (obj) {
				obj.text = obj.role_nama;
				obj.id = obj.role_id;
				return obj;
			});

			$("#select_role").select2({
				data: roles,
			});
		},
	});
}

function loadMenus() {
	var url = path + "menus/load_data/";
	$.ajax({
		type: "POST",
		url: url,
		data: { web: "true" },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Menu Null: ", err.message);
			}
			var menus = $.map(data.data, function (obj) {
				obj.text = obj.nama_idmenu;
				obj.id = obj.menu_id;
				return obj;
			});

			$("#select_menu").select2({
				data: menus,
			});

			$("#select_menu_edit").select2({
				data: menus,
				dropdownParent: $("#form_edit_access"),
				width: '-webkit-fill-available'
			});
		},
	});
}

function loadTable(all) {
	swal({
		title: "Collecting Data...",
		text: "Please wait for while...",
		imageUrl: path + "resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});

	var url = path + "access/load_data/";
	$("#tabel_access_panel").DataTable({
		destroy: true,
		paging: true,
		searching: true,
		ordering: true,
		autoWidth: true,
		processing: true,
		//"order":[[0,"desc"]],
		columnDefs: [
			{
				targets: 4, // your case first column
				className: "text-center",
				width: "5%",
			},
		],
		//"data" : data,
		ajax: {
			url: url,
			type: "POST",
			data: { scale: all },
		},
		columns: [
			{
				data: "access_id",
				visible: false,
			},
			{
				data: "role_nama",
			},
			{
				data: "nama_idmenu",
			},
			{
				data: "activ",
				render: function (data, type, row) {
					if (data == "0") {
						return "<span style='font-weight: bold;color:red;'>Inactive</span>";
					} else {
						return "<span style='font-weight: bold;color:green;'>Active</span>";
					}
				},
			},
			{
				data: null,
				render: function (data, type, row) {
					var list_action =
						"<a id='edit_detail_access' href='#' class='btn btn-info btn-xs last' " +
						"data-toggle='modal' data-target='.edit' data-backdrop='static' " +
						"data-keyboard='false' style='margin-bottom:0px;float:left;'" +
						"data-access_id='" +
						data.access_id +
						"'data-role_id='" +
						data.role_id +
						"' data-menu_id='" +
						data.menu_id +
						"' data-active='" +
						data.activ +
						"'> " +
						"<i class='fas fa-edit'></i> Edit</a> ";
					return list_action;
				},
			},
		],
	});

	swal.close();
}

$('#select_role').on("change", function () {
	var roles = $('#select_role').val();
	loadTable(roles);
});

$("#form_edit_access").submit(function () {
	var access_id = $(".modal-body #access_id").val();
	var menu_id = $(".modal-body #select_menu_edit").val();
	var active = $(".modal-body #active").val();
	var url = path + "access/updatedata";
	$.ajax({
		type: "POST",
		url: url,
		data: { access_id: access_id, role_id: ROLEID, menu_id: menu_id, active: active },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Access Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{ title: "Success", text: "Edit Data Access Success", type: "success" },
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Edit Data Access Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

$('#add_access').on("click", function () {
	var roles = $('#select_role').val();
	var menus = $('#select_menu').val();
	var url = path + "access/registerdata";

	swal({
		title: "Creating Data...",
		text: "Please wait for while...",
		imageUrl: path + "resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});

	if (menus == null || menus == []) {
		swal(
			{
				title: "List Menu Empty",
				text: "Please Select Menu Option.",
				type: "warning",
			}
		);
	} else if (roles == 0) {
		swal(
			{
				title: "Role Empty",
				text: "Please Select Role Option.",
				type: "warning",
			}
		);
	} else {
		$.ajax({
			type: "POST",
			url: url,
			data: { role_id: roles, listmenu: JSON.stringify(menus) },
			success: function (msg) {
				var data = [];
				try {
					data = JSON.parse(msg);
				} catch (err) {
					console.log("Data Access Null: ", err.message);
				}
				if (data.note == "Success") {
					swal(
						{ title: "Success", text: "Add New Access Success", type: "success" },
						function () {
							window.location = window.location.href;
						}
					);
				} else {
					swal(
						{
							title: "Error",
							text: "Add New Access Failed - " + data.note,
							type: "error",
						},
						function () {
							window.location = window.location.href;
						}
					);
					console.log("Error getting documents: ", data);
				}
			},
		});
	}
});
