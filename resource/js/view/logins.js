var path = null;

$(document).ready(function () {
	path = getpath();
});

function removeSpaces(string) {
	return string.split(" ").join("");
}

$("#form_login").submit(function () {
	var usernama = $(".login #userlogin").val();
	var pass = $(".login #passlogin").val();
	var url = path + "login/akses";
	$.ajax({
		type: "POST",
		url: url,
		data: { usernama: usernama, pass: pass },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data User Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{ title: "Success", text: "Login Account Success.", type: "success" },
					function () {
						window.location = path + "dashboard";
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Login Account Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

$("#form_register_account").submit(function () {
	var usernama = $(".tambah #user_nama").val();
	var email = $(".tambah #email_add").val();
	var pass = $(".tambah #user_pass").val();
	var pass_confirm = $(".tambah #pass_confirm").val();
	var url = path + "users/new_register";

	if(pass === pass_confirm){
		swal({
			title: "Creating Account...",
			text: "Please wait for while...",
			imageUrl: path+"resource/img/ajax-loader.gif",
			showConfirmButton: false,
			allowOutsideClick: false
		});

		$.ajax({
			type: "POST",
			url: url,
			data: { usernama: usernama, pass: pass, email: email },
			success: function (msg) {
				var data = [];
				try {
					data = JSON.parse(msg);
				} catch (err) {
					console.log("Data User Null: ", err.message);
				}
				if (data.note == "Success") {
					swal(
						{ title: "Success", text: "Register Account Success.", type: "success" },
						function () {
							window.location = window.location.href;
						}
					);
				} else {
					swal(
						{
							title: "Error",
							text: "Register Account Failed - " + data.note,
							type: "error",
						},
						function () {
							window.location = window.location.href;
						}
					);
					console.log("Error getting documents: ", data);
				}
			},
		});
	}else{
		swal(
			{
				title: "Password Not Match",
				text: "Register Account Failed",
				type: "warning",
			}
		);
	}
	return false;
});
