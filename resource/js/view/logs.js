$(document).ready(function () {
	loadTable();
});

function loadTable() {
	var url = path + "logs/load_data/";
	$("#tabel_logs").DataTable({
		destroy: true,
		paging: true,
		searching: true,
		ordering: true,
		autoWidth: true,
		processing: true,
		order: [[0, "desc"]],
		ajax: {
			url: url,
			type: "POST",
			data: { web: "true" },
		},
		columns: [
			{
				data: "id_log",
				visible: false,
			},
			{
				data: "desk_log",
			},
			{
				data: "timestamp",
				render: function (data, type, row) {
					var year = data.substring(0, 4);
					var month = data.substring(4, 6);
					var day = data.substring(6, 8);
					var hour = data.substring(8, 10);
					var minute = data.substring(10, 12);
					return year + "-" + month + "-" + day + " " + hour + ":" + minute;
				},
			},
		],
	});
}
