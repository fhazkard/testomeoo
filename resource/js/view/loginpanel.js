$(document).ready(function () {
	loadTable();
	loadRoles();
});

$(document).on("click", "#edit_detail_login", function () {
	var user_id = $(this).data("user_id");
	$(".modal-body #user_id").val(user_id);

	var user_nama = $(this).data("user_nama");
	$(".modal-body #user_nama_edit").val(user_nama);

	var email = $(this).data("email");
	$(".modal-body #email_edit").val(email);

	var user_role = $(this).data("user_role");
	$(".modal-body #user_role_edit").val(user_role);
});

$("#form_tambah_account").submit(function () {
	var nama = $(".modal-body #user_nama_add").val();
	var email = $(".modal-body #email_add").val();
	var role = $(".modal-body #user_role_add").val();
	var url = path + "users/registerdata";
	$.ajax({
		type: "POST",
		url: url,
		data: { usernama: nama, email: email, role_id: role },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Account Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{
						title: "Success",
						text: "Add New Account Success",
						type: "success",
					},
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Add New Account Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

$("#form_edit_account").submit(function () {
	var id = $(".modal-body #user_id").val();
	var nama = $(".modal-body #user_nama_edit").val();
	var email = $(".modal-body #email_edit").val();
	var role = $(".modal-body #user_role_edit").val();
	var url = path + "users/updatedata";
	$.ajax({
		type: "POST",
		url: url,
		data: { user_id: id, usernama: nama, email: email, role_id: role },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Account Null: ", err.message);
			}
			if (data.note == "Success") {
				swal(
					{
						title: "Success",
						text: "Edit Data Account Success",
						type: "success",
					},
					function () {
						window.location = window.location.href;
					}
				);
			} else {
				swal(
					{
						title: "Error",
						text: "Edit Data Account Failed - " + data.note,
						type: "error",
					},
					function () {
						window.location = window.location.href;
					}
				);
				console.log("Error getting documents: ", data);
			}
		},
	});
	return false;
});

function loadRoles() {
	var url = path + "roles/load_data/";
	$.ajax({
		type: "POST",
		url: url,
		data: { web: "true" },
		success: function (msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			} catch (err) {
				console.log("Data Role Null: ", err.message);
			}
			$.each(data.data, function (i, item) {
				$("#user_role_add").append(
					$("<option>", {
						value: item.role_id,
						text: item.role_nama,
					})
				);
				$("#user_role_edit").append(
					$("<option>", {
						value: item.role_id,
						text: item.role_nama,
					})
				);
			});
		},
	});
}

function loadTable() {
	var url = path + "users/load_data/";
	$("#tabel_login_panel").DataTable({
		destroy: true,
		paging: true,
		searching: true,
		ordering: true,
		autoWidth: true,
		processing: true,
		order: [[0, "desc"]],
		columnDefs: [
			{
				targets: 5, // your case first column
				className: "text-left",
				width: "30%",
			},
		],
		//"data" : data,
		ajax: {
			url: url,
			type: "POST",
			data: { web: "true" },
		},
		columns: [
			{
				data: "user_id",
				visible: false,
			},
			{
				data: "user_nama",
			},
			{
				data: "email",
			},
			{
				data: "role_nama",
			},
			{
				data: "active",
				render: function (data, type, row) {
					if (data == "0") {
						return "<span style='font-weight: bold;color:red;'>Inactive</span>";
					} else {
						return "<span style='font-weight: bold;color:green;'>Active</span>";
					}
				},
			},
			{
				data: null,
				render: function (data, type, row) {
					var list_action =
						"<a id='edit_detail_login' href='#' class='btn btn-info btn-xs last' " +
						"data-toggle='modal' data-target='.edit' data-backdrop='static' " +
						"data-keyboard='false' style='margin-bottom:0px;float:left;'" +
						"data-user_id='" +
						data.user_id +
						"' data-user_nama='" +
						data.user_nama +
						"' data-email='" +
						data.email +
						"' data-user_role='" +
						data.user_role +
						"'> " +
						"<i class='fas fa-edit'></i> Edit</a> ";
					var form_uniq = "form_repas_login_" + data.user_id;
					var id = '"' + data.user_id + '"';

					list_action =
						list_action +
						" <form id=" +
						form_uniq +
						" style='margin-bottom:0px;float:left;'>" +
						"<input type='hidden' name='tabel_id' value='" +
						data.user_id +
						"'>" +
						"<button class='btn btn-warning btn-xs last' onclick='confirm_repas(" +
						id +
						")' ><i class='fas fa-sync-alt'></i> Reset Password</button></form>";

					if (data.active == "0") {
						form_uniq = "form_active_login_" + data.user_id;
						id = '"' + data.user_id + '"';
						return (
							list_action +
							" <form id=" +
							form_uniq +
							" >" +
							"<input type='hidden' name='tabel_id' value='" +
							data.user_id +
							"'>" +
							"<input type='hidden' name='status' value='1'>" +
							"<button class='btn btn-success btn-xs last' onclick='confirm_active(" +
							id +
							")' style='padding-left:14px;' ><i class='fas fa-check'></i> Active  Account</button></form>"
						);
					} else {
						form_uniq = "form_suspend_login_" + data.user_id;
						id = '"' + data.user_id + '"';
						return (
							list_action +
							" <form id=" +
							form_uniq +
							" >" +
							"<input type='hidden' name='tabel_id' value='" +
							data.user_id +
							"'>" +
							"<input type='hidden' name='status' value='0'>" +
							"<button class='btn btn-danger btn-xs last' onclick='confirm_suspend(" +
							id +
							")' style='padding-left:14px;' ><i class='fa fa-times-circle'></i> Suspend  Account</button></form>"
						);
					}
				},
			},
		],
	});
}
